# Warehouse CLI C#-Template

This is the template project for C# development in the Festo Spring School. It should provide you a good starting point with some nice features built-in, which we'll explain in more detail as soon as the course starts. So far you'll get some hints here below for the most crucial commands and language specific topics.

- Gitpod configuration with working VSCode installation in Gitpod
- Testing with NUnit, FluentAssertions and Moq (for mocking)
- Gitlab Pipeline to automate and track tests

## Setup

You can either use:
- The [GitPod](https://gitpod.io) integration which will spawn a VSCode in browser or you can link your local VSCode to the one Gitpod instance.
- Your own IDE and clone the repository to your local drive

Here are some basic commands to use dotnet via command line: [dotnet CLI help](https://learn.microsoft.com/en-us/dotnet/core/tools/)

The most important are probably:

- `dotnet build` compile the whole solution
- `dotnet clean` clears all build directories
- `dotnet test` to run all tests of the current solution
- `dotnet restore` restores all packages and install those which are missing
- `dotnet add package <package_name>` if you want to install a new package for a project (you need to be in the project dir)

## Getting Started

We provided you with some basic command line application as a starting point and some tests to show how it could be done. Feel free to implement your own structure. Nevertheless we suggest to keep the existing main folder structure:

- `warehouse.cli`: This is the module where you can start implementing your own logic and create your own structure underneath
- `test.warehouse.cli`: Here you'll put the tests

## Testing

As a default testing library we added the dependency and some example tests for [NUnit](https://nunit.org/) ([Documentation](https://docs.nunit.org/articles/nunit/intro.html)). Writing asserts that are better readable and therefore sometimes better to understand can be done via [FluentAssertions](https://fluentassertions.com) ([Documentation](https://fluentassertions.com/introduction)) and if you need to mock away a REST service you could for example use [Moq](https://github.com/moq/moq4) ([Documentation](https://github.com/Moq/moq4/wiki/Quickstart)).