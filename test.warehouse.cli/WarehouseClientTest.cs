﻿using FluentAssertions;
using System.Security.Policy;
using warehouse.cli.Client;
using warehouse.cli.Enums;
using warehouse.cli.Model;


namespace test.warehouse.cli
{
    [TestFixture]
    class WarehouseClientTest
    {
        private WarehouseClient _warehouseClient;

        [SetUp]
        public void SetUp()
        {
            _warehouseClient = new WarehouseClient();
        }

        [Test]
        public void Test_MovementCommand()
        {
            List<string> directions = new List<string>();
            directions.Add("north");
            directions.Add("east");
            directions.Add("south");
            directions.Add("west");

            RobotMovementResponse response;
            foreach (string direction in directions)
            {
                response = _warehouseClient.MoveRobot(SessionNames.deborah, direction);
                Assert.IsNotNull(response);
                Assert.IsNotNull(response.RequestStatus);
                Assert.IsNotNull(response.Message);
                Assert.IsTrue(string.Equals("failed", response.RequestStatus) ||
                              string.Equals("ok", response.RequestStatus));
            }

            // check invalid direction
            response = _warehouseClient.MoveRobot(SessionNames.deborah, "southeast");
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.RequestStatus);
            Assert.IsNotNull(response.Message);
            Assert.IsTrue(string.Equals("failed", response.RequestStatus));
        }

        [Test]
        public void ScanFarCommandsWorks()
        {
            var response = _warehouseClient.CheckRobotRoomAvailability("john");
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.RequestStatus);
            Assert.IsNotNull(response.AvailabeRooms);
            Assert.IsTrue(string.Equals("ok", response.RequestStatus));
        }

        [Test]
        public void ResetWarehouseStateTest()
        {
            var response = _warehouseClient.ResetWarehouseState("john");
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.RequestStatus);
            Assert.IsNotNull(response.ResetMessage);
            Assert.IsTrue(string.Equals("done", response.ResetMessage));
            Assert.IsTrue(string.Equals("ok", response.RequestStatus));
        }

        [Test]
        public void LocateByJamesTest()
        {
            var response = _warehouseClient.Locate(SessionNames.john);
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.RequestStatus);
            Assert.IsNotNull(response.Coordinate);
            Assert.IsNotNull(response.Field);
            Assert.IsNotNull(response.MapId);
            Assert.IsNotNull(response.SessionId);
            Assert.IsTrue(string.Equals("john", response.SessionId));
            Assert.IsTrue(string.Equals("ok", response.RequestStatus));
            Assert.IsTrue(string.Equals("Field", response.Field));
            Assert.IsTrue(string.Equals("Default", response.MapId));
        }
    }
}
