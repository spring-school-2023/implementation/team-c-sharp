﻿using warehouse.cli.utils;

namespace test.warehouse.cli.utils
{
    internal class WarehouseAsciiPreviewTest
    {
        string correctMap = "   0 1 2\n  +-+-+-+\n0 |A    |\n  + +-+ +\n1 |     |\n  +-+ +-+\n2   |R|\n    +-   \n";

        [Test]
        public void CountInitialMapDeadEndTile()
        {
            string map = new WarehouseAsciiPreview().Draw(DataGenerator.InitialMapLayout);
            Assert.AreEqual(correctMap, map);
        }
    }
}
