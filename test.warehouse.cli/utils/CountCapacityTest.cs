﻿using test.warehouse.cli.TestData;
using warehouse.cli.utils;

namespace test.warehouse.cli.utils
{
    internal class CountCapacityTest
    {
        private CapacityCalculator calculator;
        private TileGenerator testData;

        [SetUp]
        public void SetUp()
        {
            calculator = new CapacityCalculator();
            testData = new TileGenerator();
        }

        [Test]
        public void CountMapTilesTest()
        {
            var map = DataGenerator.InitialMapLayout;
            Assert.AreEqual(49, calculator.GetCapacity(map));
        }

        [Test]
        public void CountXCrosingTile()
        {
            Assert.AreEqual(4, calculator.GetCapacity(testData.XCrossingTile));
            Assert.AreNotEqual(4, calculator.GetCapacity(testData.TCrossingTile));
            Assert.AreNotEqual(4, calculator.GetCapacity(testData.CornerTile));
            Assert.AreNotEqual(4, calculator.GetCapacity(testData.HallWayTile));
            Assert.AreNotEqual(4, calculator.GetCapacity(testData.DeadEndTile));
        }

        [Test]
        public void CountTCrosingTile()
        {
            Assert.AreNotEqual(6, calculator.GetCapacity(testData.XCrossingTile));
            Assert.AreEqual(6, calculator.GetCapacity(testData.TCrossingTile));
            Assert.AreNotEqual(6, calculator.GetCapacity(testData.CornerTile));
            Assert.AreNotEqual(6, calculator.GetCapacity(testData.HallWayTile));
            Assert.AreNotEqual(6, calculator.GetCapacity(testData.DeadEndTile));
        }

        [Test]
        public void CountCornerTile()
        {
            Assert.AreNotEqual(9, calculator.GetCapacity(testData.XCrossingTile));
            Assert.AreNotEqual(9, calculator.GetCapacity(testData.TCrossingTile));
            Assert.AreEqual(9, calculator.GetCapacity(testData.CornerTile));
            Assert.AreNotEqual(9, calculator.GetCapacity(testData.HallWayTile));
            Assert.AreNotEqual(9, calculator.GetCapacity(testData.DeadEndTile));
        }

        [Test]
        public void CountHallwayTile()
        {
            Assert.AreNotEqual(8, calculator.GetCapacity(testData.XCrossingTile));
            Assert.AreNotEqual(8, calculator.GetCapacity(testData.TCrossingTile));
            Assert.AreNotEqual(8, calculator.GetCapacity(testData.CornerTile));
            Assert.AreEqual(8, calculator.GetCapacity(testData.HallWayTile));
            Assert.AreNotEqual(8, calculator.GetCapacity(testData.DeadEndTile));
        }

        [Test]
        public void CountDeadEndTile()
        {
            Assert.AreNotEqual(12, calculator.GetCapacity(testData.XCrossingTile));
            Assert.AreNotEqual(12, calculator.GetCapacity(testData.TCrossingTile));
            Assert.AreNotEqual(12, calculator.GetCapacity(testData.CornerTile));
            Assert.AreNotEqual(12, calculator.GetCapacity(testData.HallWayTile));
            Assert.AreEqual(12, calculator.GetCapacity(testData.DeadEndTile));
        }

        [Test]
        public void CountInitialMapDeadEndTile()
        {
            Assert.AreNotEqual(50, calculator.GetCapacity(DataGenerator.InitialMapLayout));           
        }
    }
}
