﻿using warehouse.cli.utils;

namespace test.warehouse.cli.utils
{
    [TestFixture]
    public class StateStorageTest
    {
        private readonly IStateStorage _stateStorage = new StateStorage();

        [Test]
        public void Test_StateStorage()
        {
            var sessionName = "Tessa";

            _stateStorage.SaveState(DataGenerator.InitialMapLayout, sessionName);
            var data = _stateStorage.LoadState(sessionName);

            Assert.IsTrue(data.MapLayout.Any());
        }
    }
}
