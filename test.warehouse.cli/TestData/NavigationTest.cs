﻿using warehouse.cli.utils;

namespace test.warehouse.cli.TestData
{
    [TestFixture]
    internal class NavigationTest
    {
        [Test]
        public void NavigateToAvailableTile()
        {
            var session = "john";
            var direction = "south";
            var stateStorage = new StateStorage();
            var mapState = stateStorage.LoadState(session);
            var result = Navigation.IsLocalMovementPossible(direction, mapState);
            Assert.IsTrue(result);
        }

        [Test]
        public void NavigateToWall()
        {
            var session = "john";
            var direction = "west";
            var stateStorage = new StateStorage();
            var mapState = stateStorage.LoadState(session);
            var result = Navigation.IsLocalMovementPossible(direction, mapState);
            Assert.IsFalse(result);
        }
    }
}
