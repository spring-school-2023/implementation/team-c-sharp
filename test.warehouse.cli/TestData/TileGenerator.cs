﻿using warehouse.cli.model;
using warehouse.cli.Model;

namespace test.warehouse.cli.TestData
{
    public class TileGenerator
    {
        public Tile XCrossingTile => new()
        { 
            Capacity = 4, 
            Directions = new Directions 
            { 
                East = true, 
                North = true, 
                West = true, 
                South = true 
            }, 
            IsRamp = false,
            Coordinates = new Coordinates
            {
                XCord = 0,
                YCord = 0
            }
        };
        public Tile TCrossingTile => new()
        {
            Capacity = 6,
            Directions = new Directions
            {
                East = true,
                North = false,
                West = true,
                South = true
            },
            IsRamp = false,
            Coordinates = new Coordinates
            {
                XCord = 0,
                YCord = 0
            }
        };
        public Tile CornerTile => new()
        {
            Capacity = 9,
            Directions = new Directions
            {
                East = true,
                North = false,
                West = false,
                South = true
            },
            IsRamp = false,
            Coordinates = new Coordinates
            {
                XCord = 0,
                YCord = 0
            }
        };
        public Tile HallWayTile => new()
        {
            Capacity = 8,
            Directions = new Directions
            {
                East = false,
                North = true,
                West = false,
                South = true
            },
            IsRamp = false,
            Coordinates = new Coordinates
            {
                XCord = 0,
                YCord = 0
            }
        };
        public Tile DeadEndTile => new()
        {
            Capacity = 12,
            Directions = new Directions
            {
                East = false,
                North = true,
                West = false,
                South = false
            },
            IsRamp = false,
            Coordinates = new Coordinates
            {
                XCord = 0,
                YCord = 0
            }
        };
    }
}
