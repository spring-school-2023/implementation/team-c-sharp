﻿using Newtonsoft.Json;

namespace warehouse.cli.Model
{
    public class RobotMovementResponse
    {
        [JsonProperty("message")]
        internal string Message { get; set; }
        [JsonProperty("request_status")]
        internal string RequestStatus{ get; set; }
    }
}
