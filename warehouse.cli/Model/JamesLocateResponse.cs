﻿using System.Drawing;
using Newtonsoft.Json;

namespace warehouse.cli.Model
{
    public class JamesLocateResponse
    {
        [JsonProperty("position")]
        public LocateCoordinate Coordinate { get; set; }
        [JsonProperty("field")]
        public string Field { get; set; }
        [JsonProperty("map_id")]
        public string MapId { get; set; }
        [JsonProperty("session_id")]
        public string SessionId { get; set; }
        [JsonProperty("request_status")]
        internal string RequestStatus{ get; set; }
    }
}
