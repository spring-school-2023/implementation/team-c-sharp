﻿namespace warehouse.cli.Model
{
    public class Robot
    {
        public Coordinates DefaultCoordinates { get; set; }
        public Coordinates CurrentCoordinates { get; set; }
    }
}
