﻿using Newtonsoft.Json;

namespace warehouse.cli.Model
{
    internal class AvailableRooms
    {
        [JsonProperty("north")]
        public int North { get; set; }
        [JsonProperty("east")]
        public int East { get; set; }
        [JsonProperty("south")]
        public int South { get; set; }
        [JsonProperty("west")]
        public int West { get; set; }
    }
}
