﻿using Newtonsoft.Json;

namespace warehouse.cli.Model
{
    internal class WarehouseResetResponse
    {
        [JsonProperty("request_status")]
        public string RequestStatus { get; set; }
        [JsonProperty("message")]
        public string ResetMessage {get;set;}
    }
}
