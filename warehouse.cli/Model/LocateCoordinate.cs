﻿using Newtonsoft.Json;

namespace warehouse.cli.Model
{
    public class LocateCoordinate
    {
        [JsonProperty("x")]
        public int X { get; set; }
        [JsonProperty("y")]
        public int Y { get; set; }

        public override string ToString()
        {
            return X + "," + Y;
        }
    }
}
