﻿namespace warehouse.cli.Model
{
    public class Coordinates
    {
        public int XCord { get; set; }
        public int YCord { get; set; }
    }
}
