﻿using Newtonsoft.Json;

namespace warehouse.cli.Model
{
    internal class FarScanResponse
    {
        [JsonProperty("request_status")]
        public string RequestStatus { get; set; }
        [JsonProperty("scan_info")]
        public AvailableRooms AvailabeRooms {get;set;}
    }
}
