﻿using warehouse.cli.model;

namespace warehouse.cli.Model
{
    public class WarehouseState
    {
        public Robot Robot { get; set; }
        public List<Tile> MapLayout { get; set; } 
    }
}
