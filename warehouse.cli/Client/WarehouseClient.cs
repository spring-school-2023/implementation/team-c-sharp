﻿using System.Drawing;
using Newtonsoft.Json;
using warehouse.cli.Enums;
using warehouse.cli.Model;

namespace warehouse.cli.Client
{
    internal class WarehouseClient
    {
        private readonly string _baseAPIURL = "http://springschool-lb-54580289.eu-central-1.elb.amazonaws.com/";

        private HttpClient warehouseClient => new() { BaseAddress = new Uri(_baseAPIURL) };

        public RobotMovementResponse MoveRobot(SessionNames sessionName, string moveDirection)
        {
            var response = warehouseClient.PutAsync($"api/bot/{sessionName}/move/{moveDirection}", null).Result;

            return JsonConvert.DeserializeObject<RobotMovementResponse>(response.Content.ReadAsStringAsync().Result);
        }

        public FarScanResponse CheckRobotRoomAvailability(string sessionName)
        {
            var response = warehouseClient.GetAsync($"api/bot/{sessionName}/scan/far").Result;

            return JsonConvert.DeserializeObject<FarScanResponse>(response.Content.ReadAsStringAsync().Result);
        }

        public WarehouseResetResponse ResetWarehouseState(string sessionName)
        {
            var response = warehouseClient.PutAsync($"api/james/{sessionName}/reset", null).Result;

            return JsonConvert.DeserializeObject<WarehouseResetResponse>(response.Content.ReadAsStringAsync().Result);
        }

        public JamesLocateResponse Locate(SessionNames sessionName)
        {
            var response = warehouseClient.GetAsync($"api/james/{sessionName}/locate").Result;

            return JsonConvert.DeserializeObject<JamesLocateResponse>(response.Content.ReadAsStringAsync().Result);
        }
    }
}
