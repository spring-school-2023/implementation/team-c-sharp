﻿using warehouse.cli.model;
using warehouse.cli.Model;

namespace warehouse.cli.utils
{
    public class CapacityCalculator
    {
        public int GetCapacity(Tile tile)
        {
            if (IsRampTile(tile)) return 0;
            else if (IsXCrosingTile(tile)) return 4;
            else if (IsTCrossingTile(tile)) return 6;
            else if (IsCornerTile(tile)) return 9;
            else if (IsHallwayTile(tile)) return 8;
            else if (IsDeadEndTile(tile)) return 12;
            else throw new Exception("Unknown Tile");
        }

        public int GetCapacity(WarehouseState map)
        {
            int totalCapacity = 0;
            foreach (var tile in map.MapLayout)
                totalCapacity += GetCapacity(tile);

            return totalCapacity;
        }
        private bool IsRampTile(Tile tile)
        {
            if (tile.IsRamp) return true;
            else return false;
        }


        private bool IsTCrossingTile(Tile tile)
        {
            if (tile.Directions.North && tile.Directions.East && tile.Directions.South && !tile.Directions.West) return true;
            else if (tile.Directions.North && tile.Directions.East && !tile.Directions.South && tile.Directions.West) return true;
            else if (tile.Directions.North && !tile.Directions.East && tile.Directions.South && tile.Directions.West) return true;
            else if (!tile.Directions.North && tile.Directions.East && tile.Directions.South && tile.Directions.West) return true;
            else return false;
        }

        private bool IsHallwayTile(Tile tile)
        {
            if (tile.Directions.North && !tile.Directions.East && tile.Directions.South && !tile.Directions.West) return true;
            else if (!tile.Directions.North && tile.Directions.East && !tile.Directions.South && tile.Directions.West) return true;          
            else return false;
        }

        private bool IsCornerTile(Tile tile)
        {
            if (tile.Directions.North && tile.Directions.East && !tile.Directions.South && !tile.Directions.West) return true;
            else if (!tile.Directions.North && tile.Directions.East && tile.Directions.South && !tile.Directions.West) return true;
            else if (!tile.Directions.North && !tile.Directions.East && tile.Directions.South &&!tile.Directions.West) return true;
            else if (tile.Directions.North && !tile.Directions.East && !tile.Directions.South && tile.Directions.West) return true;
            else return false;
        }

        private bool IsDeadEndTile(Tile tile)
        {
            if (tile.Directions.North && !tile.Directions.East && !tile.Directions.South && !tile.Directions.West) return true;
            else if (!tile.Directions.North && tile.Directions.East && !tile.Directions.South && !tile.Directions.West) return true;
            else if (!tile.Directions.North && !tile.Directions.East && tile.Directions.South && !tile.Directions.West) return true;
            else if (!tile.Directions.North && !tile.Directions.East && !tile.Directions.South && tile.Directions.West) return true;
            else return false;  
        }

       
        private bool IsXCrosingTile(Tile tile)
        {
            return tile.Directions.North && tile.Directions.East && tile.Directions.South && tile.Directions.West;
        }
    }
}
