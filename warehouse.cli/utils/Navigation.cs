﻿using System.Reflection;
using warehouse.cli.model;
using warehouse.cli.Model;

namespace warehouse.cli.utils
{
    public static class Navigation
    {
        public static bool IsLocalMovementPossible(string direction, WarehouseState mapState)
        {
            var tile = mapState.MapLayout.FirstOrDefault(x => mapState.Robot.CurrentCoordinates.XCord == x.Coordinates.XCord &&
                       mapState.Robot.CurrentCoordinates.YCord == x.Coordinates.YCord);
            var property = tile.Directions.GetType().GetProperties().FirstOrDefault(x => x.Name.ToLower() == direction);
            return bool.Parse(property.GetValue(tile.Directions).ToString());
        }
    }
}
