﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using warehouse.cli.Enums;

namespace warehouse.cli.utils
{
    public class Helper
    {
        public static bool ValidateSessionName(string sessionId)
        {
            if (!Enum.IsDefined(typeof(SessionNames), sessionId))
            {
                Console.WriteLine("Invalid session name!");
                return false;
            }

            return true;
        }
    }
}
