﻿using warehouse.cli.Model;

namespace warehouse.cli.utils
{
    public interface IStateStorage
    {
        WarehouseState LoadState(string sessionName);
        void SaveState(WarehouseState warehouseState, string sessionName);
    }
}
