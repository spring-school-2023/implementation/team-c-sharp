﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using warehouse.cli.model;
using warehouse.cli.Model;

namespace warehouse.cli.utils
{
    internal class WarehouseAsciiPreview
    {
        public string Draw(WarehouseState state)
        {
            string map = string.Empty;
            state.MapLayout.OrderBy(a => a.Coordinates.XCord).ThenBy(a => a.Coordinates.YCord);
            int yMax = state.MapLayout.Select(a => a.Coordinates.YCord).Max();
            map += DrawXLegend(state.MapLayout);
            for (int y = 0; y <= yMax; y++)
            {
                map += DrawWall(y, yMax, state.MapLayout);
                map += DrawArea(y, state.MapLayout, state.Robot.CurrentCoordinates);
            }
            map += DrawWall(yMax + 1, yMax, state.MapLayout);

            return map;
        }

        private string DrawXLegend(List<Tile> mapLayout)
        {
            var line = "  ";
            for (int x = 0; x <= mapLayout.Select(a => a.Coordinates.XCord).Max(); x++)
                line += $" {x}";

            return line + "\n";
        }

        private string DrawWall(int y, int yMax, List<Tile> mapLayout)
        {
            var line = "   ";
            int xMax = mapLayout.Select(a => a.Coordinates.XCord).Max(); //.Where(a => a.Coordinates.YCord == y)
            for (int x = 0; x <= xMax; x++)
            {
                var tile = mapLayout.Find(a => a.Coordinates.XCord == x && a.Coordinates.YCord == y);
                if (tile == null)
                {
                    var upperTile = mapLayout.Find(a => a.Coordinates.XCord == x && a.Coordinates.YCord == y - 1);
                    line = line.Substring(0, line.Length - 1) + (upperTile != null ? "+-+" : "   ");
                }
                else
                {
                    line = line.Substring(0, line.Length - 1) + (tile.Directions.North ? "+ +" : "+-+");
                }
            }

            return line + "\n";
        }

        private string DrawArea(int y, List<Tile> mapLayout, Coordinates robotCoordinates)
        {
            var line = $"{y} ";
            int xMax = mapLayout.Where(a => a.Coordinates.YCord == y).Select(a => a.Coordinates.XCord).Max();
            for (int x = 0; x <= xMax; x++)
            {
                var tile = mapLayout.Find(a => a.Coordinates.XCord == x && a.Coordinates.YCord == y);
                if (tile == null)
                {
                    line += "  ";
                }
                else
                {
                    line += !tile.Directions.West ? $"|{DrawRobotOrRamp(tile, robotCoordinates)}" : "  ";
                }
            }

            return line + "|\n";
        }

        private string DrawRobotOrRamp(Tile tile, Coordinates robot)
        {
            if (tile.Coordinates.YCord == robot.YCord && tile.Coordinates.XCord == robot.XCord)
                return "A";
            if (tile.IsRamp)
                return "R";
            return " ";
        }
    }
}
