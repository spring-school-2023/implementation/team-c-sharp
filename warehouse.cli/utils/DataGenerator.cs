﻿using warehouse.cli.model;
using warehouse.cli.Model;

namespace warehouse.cli.utils
{
    public class DataGenerator
    {
        public static WarehouseState InitialMapLayout => new()
        {
            MapLayout = new List<Tile>
                    {
                        new Tile
                        {
                            Capacity = 9,
                            Directions = new Directions
                            {
                                East = true,
                                North = false,
                                West = false,
                                South = true
                            },
                            IsRamp = false,
                            Coordinates = new Coordinates
                            {
                                XCord = 0,
                                YCord = 0
                            }
                        },
                        new Tile
                        {
                            Capacity = 8,
                            Directions = new Directions
                            {
                                East = true,
                                North = false,
                                West = true,
                                South = false
                            },
                            IsRamp = false,
                            Coordinates = new Coordinates
                            {
                                XCord = 1,
                                YCord = 0
                            }
                        },
                        new Tile
                        {
                            Capacity = 9,
                            Directions = new Directions
                            {
                                East = true,
                                North = false,
                                West = true,
                                South = false
                            },
                            IsRamp = false,
                            Coordinates = new Coordinates
                            {
                                XCord = 2,
                                YCord = 0
                            }
                        },
                        new Tile
                        {
                            Capacity = 9,
                            Directions = new Directions
                            {
                                East = true,
                                North = true,
                                West = false,
                                South = false
                            },
                            IsRamp = false,
                            Coordinates = new Coordinates
                            {
                                XCord = 0,
                                YCord = 1
                            }
                        },
                        new Tile
                        {
                            Capacity = 6,
                            Directions = new Directions
                            {
                                East = true,
                                North = false,
                                West = true,
                                South = true
                            },
                            IsRamp = false,
                            Coordinates = new Coordinates
                            {
                                XCord = 1,
                                YCord = 1
                            }
                        },
                        new Tile
                        {
                            Capacity = 9,
                            Directions = new Directions
                            {
                                East = false,
                                North = true,
                                West = true,
                                South = false
                            },
                            IsRamp = false,
                            Coordinates = new Coordinates
                            {
                                XCord = 2,
                                YCord = 1
                            }
                        },
                        new Tile
                        {
                            Capacity = 0,
                            Directions = new Directions
                            {
                                East = false,
                                North = true,
                                West = false,
                                South = false
                            },
                            IsRamp = true,
                            Coordinates = new Coordinates
                            {
                                XCord = 1,
                                YCord = 2
                            }
                        },
                    },
            Robot = new Robot
            {
                DefaultCoordinates = new Coordinates
                {
                    XCord = 0,
                    YCord = 0
                },
                CurrentCoordinates = new Coordinates
                {
                    XCord = 0,
                    YCord = 0
                }
            }
        };
    }
}
