﻿using System.Configuration;
using Newtonsoft.Json;
using warehouse.cli.Model;

namespace warehouse.cli.utils
{
    public class StateStorage : IStateStorage
    {
        private readonly string _storageFolder = ConfigurationManager.AppSettings["StorageFolder"];

        public WarehouseState LoadState(string sessionName)
        {
            try
            {
                var filePath = GetFilePath(sessionName);
                var json = File.ReadAllText(filePath);

                return JsonConvert.DeserializeObject<WarehouseState>(json);
            }
            catch
            {
                SaveState(DataGenerator.InitialMapLayout, sessionName);
                return DataGenerator.InitialMapLayout;
            }
        }

        public void SaveState(WarehouseState warehouseState, string sessionName)
        {
            var stateString = JsonConvert.SerializeObject(warehouseState);
            var filePath = GetFilePath(sessionName);
            if (!Directory.Exists(_storageFolder))
                Directory.CreateDirectory(_storageFolder);
            File.WriteAllText(filePath, stateString);
        }

        private string GetFilePath(string sessionName)
        {
            return Path.Combine(_storageFolder, $"{sessionName}.json");
        }
    }
}
