﻿using warehouse.cli.Client;
using warehouse.cli.Enums;
using warehouse.cli.utils;

namespace warehouse.cli.Commands
{
    public class GetCapacityCommand : IWarehouseCommand
    {
        public string Execute(SessionNames session, string argument = "")
        {
            try
            {
                var stateStorage = new StateStorage();
                var mapState = stateStorage.LoadState(session.ToString());
                
                CapacityCalculator c = new CapacityCalculator();
                
                return c.GetCapacity(mapState).ToString();
            }
            catch
            {
                return "-1";
            }
        }
    }
}
