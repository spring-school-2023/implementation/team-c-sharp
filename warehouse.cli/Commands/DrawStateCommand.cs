﻿using warehouse.cli.Enums;
using warehouse.cli.utils;

namespace warehouse.cli.Commands
{
    internal class DrawStateCommand : IWarehouseCommand
    {
        public string Execute(SessionNames session, string argument = "")
        {
            var stateStorage = new StateStorage();
            var mapState = stateStorage.LoadState(session.ToString());
            return new WarehouseAsciiPreview().Draw(mapState);
        }
    }
}
