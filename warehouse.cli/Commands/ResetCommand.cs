﻿using warehouse.cli.Client;
using warehouse.cli.Enums;
using warehouse.cli.utils;

namespace warehouse.cli.Commands
{
    public class ResetCommand : IWarehouseCommand
    {
        public string Execute(SessionNames session, string argument = "")
        {
            try
            {
                var sessionId = session.ToString();
                var stateStorage = new StateStorage();
                var warehouseClient = new WarehouseClient();
                var currentState = stateStorage.LoadState(sessionId);

                switch (warehouseClient.ResetWarehouseState(sessionId).RequestStatus)
                {
                    case "ok":
                        currentState.Robot.CurrentCoordinates = currentState.Robot.DefaultCoordinates;
                        stateStorage.SaveState(currentState, sessionId);
                        return Boolean.TrueString;
                    default:
                        return Boolean.FalseString;
                }
            }
            catch
            {
                return Boolean.FalseString;
            }
        }
    }
}
