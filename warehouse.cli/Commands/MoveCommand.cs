﻿using warehouse.cli.Client;
using warehouse.cli.Enums;
using warehouse.cli.Model;
using warehouse.cli.utils;

namespace warehouse.cli.Commands
{
    public class MoveCommand : IWarehouseCommand
    {
        public string Execute(SessionNames session, string argument = "")
        {
            var stateStorage = new StateStorage();
            var direction = argument;
            var mapState = stateStorage.LoadState(session.ToString());
            if(Navigation.IsLocalMovementPossible(direction, mapState))
            {
                var warehouseClient = new WarehouseClient();
                var result = warehouseClient.MoveRobot(session, direction);
                if (result.RequestStatus.ToLower().Equals("ok")) return $"Robot moved: {direction}";
                else return "Internal state is out of sync!";
            }

            return "Could not move! There's a wall in front!";
        }
    }
}
