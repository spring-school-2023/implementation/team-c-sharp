﻿using warehouse.cli.Enums;

namespace warehouse.cli.Commands
{
    public interface IWarehouseCommand
    {
        string Execute(SessionNames session, string argument = "");
    }
}
