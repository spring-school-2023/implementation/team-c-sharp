﻿using warehouse.cli.Client;
using warehouse.cli.Enums;
using warehouse.cli.utils;

namespace warehouse.cli.Commands
{
    public class LocateCommand : IWarehouseCommand
    {
        public string Execute(SessionNames session, string argument = "")
        {
            var sessionId = session.ToString();
            var stateStorage = new StateStorage();
            var warehouseClient = new WarehouseClient();
            var currentState = stateStorage.LoadState(sessionId);

            var result = warehouseClient.Locate(session);

            return result.RequestStatus == "ok" ? result.Coordinate.ToString(): string.Empty;
        }
    }
}
