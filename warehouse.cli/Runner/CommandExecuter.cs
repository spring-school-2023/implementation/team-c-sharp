﻿using warehouse.cli.Client;
using warehouse.cli.Commands;
using warehouse.cli.Enums;
using warehouse.cli.Model;

namespace warehouse.cli.Runner
{
    public class CommandExecuter
    {
        private static readonly Dictionary<string, IWarehouseCommand> _listOfCommands = new()
        {
            { "north", new MoveCommand() },
            { "east", new MoveCommand() },
            { "west", new MoveCommand()},
            { "south", new MoveCommand() },
            { "reset", new ResetCommand() },
            { "capacity", new GetCapacityCommand() },
            { "state", new DrawStateCommand() }
        };

        public static WarehouseState Run(string sessionId, WarehouseState warehouseState)
        {
            //var sessionName = SelectSession();
            var sessionName = GetSessionName(sessionId);
            ExecuteCommand(sessionName);

            // get new position by james 
            var resultLocate = new LocateCommand().Execute(sessionName);
            if (!string.IsNullOrEmpty(resultLocate))
            {
                var coord = resultLocate.Split(",");
                warehouseState.Robot.CurrentCoordinates.XCord = int.Parse(coord[0]);
                warehouseState.Robot.CurrentCoordinates.YCord = int.Parse(coord[1]);
            }

            return warehouseState;
        }

        private static void ExecuteCommand(SessionNames session)
        {
            Console.WriteLine();
            Console.WriteLine("Valid commands:");
            foreach (string command in _listOfCommands.Keys)
            {
                Console.WriteLine(command);
            }
            Console.WriteLine();

            Console.WriteLine("Please enter your command now ...");
            var input = Console.ReadLine();
            var stringCommand = input;
            if (_listOfCommands.ContainsKey(stringCommand))
            {
                var command = _listOfCommands[stringCommand];
                var result = command.Execute(session, stringCommand);
                Console.WriteLine(result);
            }
        }

        private static SessionNames SelectSession()
        {
            Console.WriteLine("Available sessions:");
            foreach (SessionNames sessionName in (SessionNames[])Enum.GetValues(typeof(SessionNames)))
            {
                Console.WriteLine(sessionName);
            }

            string answerSessionName = string.Empty;
            do
            {
                Console.WriteLine("Please enter the session name:");
                answerSessionName = Console.ReadLine();

                if (!Enum.IsDefined(typeof(SessionNames), answerSessionName))
                {
                    Console.WriteLine("Invalid session name!");
                }
            } while (!Enum.IsDefined(typeof(SessionNames), answerSessionName));

            return GetSessionName(answerSessionName);
        }

        private static SessionNames GetSessionName(string sessionId)
        {
            var selectedSession = (SessionNames)Enum.Parse(typeof(SessionNames), sessionId);
            Console.WriteLine("Used session name: " + selectedSession);
            return selectedSession;
        }

    }
}
