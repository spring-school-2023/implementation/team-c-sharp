﻿using warehouse.cli.Model;
using warehouse.cli.Runner;
using warehouse.cli.utils;

namespace festo.springschool
{

    class Program {

        public static void Main(string[] args)
        {
            var sessionId = args[0];

            //validate session name
            if (!Helper.ValidateSessionName(sessionId))
            {
                Environment.Exit(0);
            }

            var stateStorage = new StateStorage();
            var mapState = stateStorage.LoadState(sessionId);
           
            WarehouseState newMapState = CommandExecuter.Run(sessionId, mapState);

            stateStorage.SaveState(newMapState, sessionId);
        }
    }
}
