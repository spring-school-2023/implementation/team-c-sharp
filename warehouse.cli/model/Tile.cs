﻿using warehouse.cli.Model;

namespace warehouse.cli.model
{
    public class Tile
    {
        public int Capacity { get; set; }
        public bool IsRamp { get; set; }
        public Directions Directions { get; set; }
        public Coordinates Coordinates { get; set; }
    }
}
