﻿namespace warehouse.cli.model
{
    public class Directions
    {
        public bool North { get; set; }
        public bool East { get; set; }
        public bool South { get; set; }
        public bool West { get; set; }
    }
}
